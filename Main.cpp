﻿#include <iostream>
#include <ctime>

int main() {
    const int N = 5; // Размер массива
    int arr[N][N];

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            arr[i][j] = i + j;
        }
    }

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            std::cout << arr[i][j] << " ";
        }
        std::cout << std::endl;
    }

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int currentDay = buf.tm_mday;

    int rowIndex = currentDay % N;
    int rowSum = 0;
    for (int j = 0; j < N; ++j) {
        rowSum += arr[rowIndex][j];
    }
    std::cout << "Сумма элементов в строке с индексом " << rowIndex << ": " << rowSum << std::endl;

    return 0;
}
